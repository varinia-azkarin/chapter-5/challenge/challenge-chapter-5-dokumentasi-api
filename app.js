const express = require('express');
const morgan = require('morgan');
const app = express();
const port = 3000;
const router = require('./routes');
const swaggerJSON = require('./swagger.json')
const swaggerUi = require('swagger-ui-express')

app.use(morgan('dev'));
app.use(express.json());

app.use('/api/v1', router);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerJSON))

app.listen(port, () => {
    console.log('Berjalan pada Port', port);
});