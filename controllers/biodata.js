const { Biodata, User } = require('../models');

createBiodata = async (req, res) => {
    try {
        let { user_id, nickname, gender, age, description } = req.body;
        let checkBiodata = await Biodata.findOne({where: {user_id}})
        if (checkBiodata){
            res.status(409).json({
                status:'error',
                errors:'user already has biodata'
            })
            return
        }
        let user = await User.findOne({ where: { id: user_id } });
        if (!user) {
            res.status(404).json({
                status: 'error',
                errors: `user with id ${user_id} is doesn't exist!`
            });
            return;
        }
        let newBiodata = await Biodata.create({
            user_id,
            nickname,
            gender,
            age,
            description
        });

        res.status(201).json({
            status: 'success',
            message: 'succesfully create data',
            data: newBiodata
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};

getBiodatas = async (req, res) => {
    try {
        let biodatas = await Biodata.findAll();

        res.status(200).json({
            status: 'success',
            message: 'succesfully get all data',
            data: biodatas
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};

getBiodata = async (req, res) => {
    try {
        const biodata_id = req.params.id;

        let biodata = await Biodata.findOne(
            {
            where: {
                id: biodata_id
            } ,
            include:'user'
        });

        if (!biodata) {
            res.status(404).json({
                status: 'error',
                message: 'cant find biodata with id ' + biodata_id,
                data: null
            });
            return;
        }

        res.status(200).json({
            status: 'success',
            message: 'succesfully get detail data',
            data: biodata
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};

updateBiodata = async (req, res) => {
    try {
        const biodata_id = req.params.id;
        const { user_id, nickname, gender, age, description } = req.body;

        let query = {
            where: {
                id: biodata_id
            }
        };

        let updated = await Biodata.update({
            user_id,
            nickname,
            gender,
            age,
            description
        }, query);

        res.status(200).json({
            status: 'success',
            message: 'succesfully update data',
            data: updated
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};

deleteBiodata = async (req, res) => {
    try {
        const biodata_id = req.params.id;

        let deleted = await Biodata.destroy({
            where: {
                id: biodata_id
            }
        });

        res.status(200).json({
            status: 'success',
            message: 'succesfully delete data',
            data: deleted
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};



module.exports = {
    createBiodata,
    getBiodatas,
    getBiodata,
    updateBiodata,
    deleteBiodata
}