const { User } = require('../models');

createUser = async (req, res) => {
    try {
        let { username, email, password } = req.body;
        let checkUser = await User.findOne({where: {username}})
        if (checkUser){
            res.status(409).json({
                status:'error',
                errors:'username already exist'
            })
            return
        }
        let newUser = await User.create({
            username,
            email,
            password
        });

        res.status(201).json({
            status: 'success',
            message: 'succesfully create data',
            data: newUser
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};

getUsers = async (req, res) => {
    try {
        let users = await User.findAll();

        res.status(200).json({
            status: 'success',
            message: 'succesfully get all data',
            data: users
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};

getUser = async (req, res) => {
    try {
        const user_id = req.params.id;

        let user = await User.findOne(
            {
            where: {
                id: user_id
            }, 
            include:[
                'userBiodata', 'userHistory'
            ]
        });

        if (!user) {
            res.status(404).json({
                status: 'error',
                message: 'cant find user with id ' + user_id,
                data: null
            });
            return;
        }

        res.status(200).json({
            status: 'success',
            message: 'succesfully get detail data',
            data: user
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};

updateUser = async (req, res) => {
    try {
        const user_id = req.params.id;
        const { username, email, password } = req.body;

        let query = {
            where: {
                id: user_id
            }
        };

        let updated = await User.update({
            username,
            email,
            password
        }, query);

        res.status(200).json({
            status: 'success',
            message: 'succesfully update data',
            data: updated
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};

deleteUser = async (req, res) => {
    try {
        const user_id = req.params.id;

        let deleted = await User.destroy({
            where: {
                id: user_id
            }
        });

        res.status(200).json({
            status: 'success',
            message: 'succesfully delete data',
            data: deleted
        });

    } catch (err) {
        console.log(err);
        res.status(500).json({
            status: 'error',
            errors: err
        });
    }
};



module.exports = {
    createUser,
    getUsers,
    getUser,
    updateUser,
    deleteUser
}