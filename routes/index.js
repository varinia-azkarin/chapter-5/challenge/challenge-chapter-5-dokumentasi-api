const express = require('express');
const router = express.Router();
const user = require('./user')
const biodata = require('./biodata')
const history = require('./history')

router.get('/', (req, res) => {
    res.status(200).json({
        status: 'success',
        message: 'Selamat datang di EPIXGAMES',
        data: null
    });
});

router.use('/users', user)
router.use('/biodatas', biodata)
router.use('/histories', history)
module.exports = router;