const express = require('express');
const router = express.Router();

const { createHistory, getHistorys, getHistory, updateHistory, deleteHistory } = require('../controllers/history');

router.post('/', createHistory); // create user
router.get('/', getHistorys); // getAll users
router.get('/:id', getHistory); // getDetail user
router.put('/:id', updateHistory); // update user
router.delete('/:id', deleteHistory); // delete user

module.exports = router;