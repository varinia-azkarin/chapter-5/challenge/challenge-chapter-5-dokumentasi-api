const express = require('express');
const router = express.Router();

const { createBiodata, getBiodatas, getBiodata, updateBiodata, deleteBiodata } = require('../controllers/biodata');

router.post('/', createBiodata); // create user
router.get('/', getBiodatas); // getAll users
router.get('/:id', getBiodata); // getDetail user
router.put('/:id', updateBiodata); // update user
router.delete('/:id', deleteBiodata); // delete user

module.exports = router;